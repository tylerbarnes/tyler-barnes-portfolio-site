# Tyler Barnes Portfolio #
***
This repo is a static HTML/SCSS/JS project scaffolded with the [yeoman](http://yeoman.io/) [gulp-webapp generator](https://github.com/yeoman/generator-gulp-webapp) and uses [fullPage.js](https://github.com/alvarotrigo/fullPage.js/) by [Alvaro Trigo](https://github.com/alvarotrigo). This is a portfolio site for my personal projects and will likely not be useful to anyone else though you are free to look through the code and reuse it if you wish to.
 
When all features are in a useable state, the production webapp will be built and pushed to [my bitbucket page.](https://tylerbarnes.bitbucket.org)



### How do I get set up? ###
***
Go to the [gulp-webapp repo page](https://github.com/yeoman/generator-gulp-webapp) and check out their readme.