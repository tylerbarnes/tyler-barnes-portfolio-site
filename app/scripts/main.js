/*global $*/
(function () {
  'use strict';
  var speed = 200;
  
  $(document).ready(function () {
//    $('#pagepiling').pagepiling();
    $('#fullpage').fullpage({scrollOverflow: true});
    $.fn.fullpage.setAllowScrolling(false);
    $.fn.fullpage.setKeyboardScrolling(false);
    $('#intro .button, #work-nav-button, #intro h1 span').on('click', function () {
      $('#about, #contact').hide();
      $('#projects').show();
      $.fn.fullpage.moveSectionDown();
      $.fn.fullpage.setAllowScrolling(true);
      $.fn.fullpage.setKeyboardScrolling(true);
    });
    $('#about-nav-button').on('click', function () {
//      $('#about').fadeIn(speed);
      $('#projects, #contact').hide();
      $('#about').show();
      $.fn.fullpage.moveSectionDown();
      $.fn.fullpage.setAllowScrolling(true);
      $.fn.fullpage.setKeyboardScrolling(true);
    });
//    $('#about .close').on('click', function () {
//      $('#about').fadeOut(speed);
//    });
    $('#contact-nav-button').on('click', function () {
//      $('#contact').fadeIn(speed);
      $('#projects, #about').hide();
      $('#contact').show();
      $.fn.fullpage.moveSectionDown();
      $.fn.fullpage.setAllowScrolling(true);
      $.fn.fullpage.setKeyboardScrolling(true);
    });
//    $('#contact .close').on('click', function () {
//      $('#contact').fadeOut(speed);
//    });
  });
}());